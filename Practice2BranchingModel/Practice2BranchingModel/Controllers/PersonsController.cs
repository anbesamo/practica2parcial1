﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Practice2BranchingModel.Controllers
{
    [ApiController]
    [Route("/api/persons")]
    public class PersonsController : ControllerBase
    {

        Random r = new Random();
        public PersonsController()
        {
           
        }

        [HttpDelete]
        public Person DeletePerson([FromBody] Person p )
        {
            p.Name = "Deleted";
            p.LastName = "Deleted";
            p.Age = 0;

            return p;
        }

        [HttpGet]
        public List<Person> GetPersons()
        {
            List<Person> people = new List<Person>();
            for (int i=0;i<20;i++) 
            {
                string[] Names = {"John","Mikhail","Sarah" ,"Luke","David","Camile","Damon","Grace","Hope","Luis"};
                string[] LastNames = { "Mikhaelson", "Stark", "Glaymore", "Dtuchin", "Hellvan", "Sarmiento", "Torrez", "Crist", "Evans", "Weasley" };
             
         
                Person p = new Person();
                p.Name = Names[r.Next(0, 9)];
                p.LastName = LastNames[r.Next(0, 9)];
                p.Age = r.Next(20, 65);

                if (!people.Contains(p)) 
                {
                    people.Add(p);
                }
                


            }

            

            return people;
        }

        [HttpPost]
        public Person CreatePerson([FromBody] string pname, [FromBody] string lastname, [FromBody] int age)
        {
            return new Person() { Name=pname, LastName = lastname , Age = age} ;
        }

       
        [HttpPut]
        public Person UpdatePerson([FromBody]Person p)
        {
            p.Name = "Updated-Name";
            p.LastName = "Updated=LastName";
            p.Age = 100;
            return p;
        }
    }
}
