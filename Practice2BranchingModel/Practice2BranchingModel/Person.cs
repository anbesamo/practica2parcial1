using System;

namespace Practice2BranchingModel
{
    public class Person
    {
       
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
